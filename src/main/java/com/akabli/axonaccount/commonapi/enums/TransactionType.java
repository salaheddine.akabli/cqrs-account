package com.akabli.axonaccount.commonapi.enums;

public enum TransactionType {
    DEBIT, CREDIT
}
