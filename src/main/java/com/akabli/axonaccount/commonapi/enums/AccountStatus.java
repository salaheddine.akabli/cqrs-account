package com.akabli.axonaccount.commonapi.enums;

public enum AccountStatus {

    CREATED, ACTIVATED, SUSPENDED
}
