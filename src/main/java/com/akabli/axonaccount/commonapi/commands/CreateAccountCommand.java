package com.akabli.axonaccount.commonapi.commands;

import lombok.Getter;


public class CreateAccountCommand extends BaseCommand<String>{
    @Getter
    private final String currency;
    @Getter
    private final double initialeBalance;

    public CreateAccountCommand(String id, String currency, double initialeBalance) {
        super(id);
        this.currency = currency;
        this.initialeBalance = initialeBalance;
    }
}
