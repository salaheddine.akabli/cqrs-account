package com.akabli.axonaccount.commonapi.commands;

import lombok.Getter;

public class DebitAccountCommand extends BaseCommand<String>{
    @Getter
    private final String currency;
    @Getter
    private final double amount;

    public DebitAccountCommand(String id, String currency, double amount) {
        super(id);
        this.currency = currency;
        this.amount = amount;
    }
}
