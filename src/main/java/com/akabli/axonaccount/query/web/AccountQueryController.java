package com.akabli.axonaccount.query.web;

import com.akabli.axonaccount.query.entities.Account;
import com.akabli.axonaccount.query.queries.GetAllAccounts;
import lombok.AllArgsConstructor;
import org.axonframework.messaging.responsetypes.ResponseType;
import org.axonframework.messaging.responsetypes.ResponseTypes;
import org.axonframework.queryhandling.QueryGateway;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/query/account")
@AllArgsConstructor
public class AccountQueryController {

    private QueryGateway queryGateway;

    @GetMapping("/accounts")
    public List<Account> getAll() throws ExecutionException, InterruptedException {
        CompletableFuture<List<Account>> query = queryGateway.query(new GetAllAccounts(), ResponseTypes.multipleInstancesOf(Account.class));
        return  query.get();
    }

}
