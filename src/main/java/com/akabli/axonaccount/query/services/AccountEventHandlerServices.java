package com.akabli.axonaccount.query.services;

import com.akabli.axonaccount.commonapi.events.AccountCreatedEvent;
import com.akabli.axonaccount.commonapi.events.AccountCreditedEvent;
import com.akabli.axonaccount.query.entities.Account;
import com.akabli.axonaccount.query.queries.GetAllAccounts;
import com.akabli.axonaccount.query.repositories.AccountRepository;
import com.akabli.axonaccount.query.repositories.AccountTransactionRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.EventMessage;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
@Slf4j
@AllArgsConstructor
public class AccountEventHandlerServices {
    private final AccountRepository accountRepository;
    private final AccountTransactionRepository accountTransactionRepository;

    @EventHandler
    public void on(AccountCreatedEvent event, EventMessage<AccountCreatedEvent> message){
        log.info("*********** AccountRepository received ***************");
        Account account = new Account();
        account.setId(event.getId());
        account.setBalance(event.getBalance());
        account.setStatus(event.getStatus());
        account.setCurrency(event.getCurrency());
        account.setCreatedDate(message.getTimestamp());
        accountRepository.save(account);
    }

    @QueryHandler
    public List<Account> on(GetAllAccounts query){
        return accountRepository.findAll();
    }
}
