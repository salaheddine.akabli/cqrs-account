package com.akabli.axonaccount.query.repositories;

import com.akabli.axonaccount.query.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account, String> {
}
