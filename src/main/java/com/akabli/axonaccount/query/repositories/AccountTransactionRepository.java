package com.akabli.axonaccount.query.repositories;

import com.akabli.axonaccount.query.entities.AccountTransaction;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountTransactionRepository extends JpaRepository<AccountTransaction, String> {
}
