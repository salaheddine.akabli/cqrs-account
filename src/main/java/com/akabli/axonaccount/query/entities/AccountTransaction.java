package com.akabli.axonaccount.query.entities;

import com.akabli.axonaccount.commonapi.enums.TransactionType;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Entity
@AllArgsConstructor @NoArgsConstructor @Data
public class AccountTransaction {
    @Id
    private String id;
    private Date timeStamp;
    private double amount;
    @ManyToOne
    private Account account;
    private TransactionType transactionType;
}
