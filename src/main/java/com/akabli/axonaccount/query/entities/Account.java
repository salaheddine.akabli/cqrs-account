package com.akabli.axonaccount.query.entities;

import com.akabli.axonaccount.commonapi.enums.AccountStatus;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "account")
@AllArgsConstructor @NoArgsConstructor @Data
public class Account {
    @Id
    private String id;
    private String currency;
    private Instant createdDate;
    private double balance;
    @Enumerated(EnumType.STRING )
    private AccountStatus status;
    @OneToMany(mappedBy = "account")
    private List<AccountTransaction> transactions;
}
