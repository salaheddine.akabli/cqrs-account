package com.akabli.axonaccount.command.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor @NoArgsConstructor @Data
public class CreateAccoutnDTO {
    private String currency;
    private double balance;
}
