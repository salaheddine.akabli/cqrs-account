package com.akabli.axonaccount.command.exceptions;

import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
@AllArgsConstructor
public class ExceptionControllerAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleExceptiob(Exception ex) {
        return new ErrorResponse(ex.getMessage());
    }

    @ExceptionHandler(BalanceNegativeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ErrorResponse handleBlanceInitialeException(Exception ex) {
        return new ErrorResponse(ex.getMessage());
    }
    public static class BalanceNegativeException extends RuntimeException {
        public BalanceNegativeException(String message) {
            super(message);
        }
    }

    @AllArgsConstructor
    class ErrorResponse {
        String errorMessage;
    }
}
