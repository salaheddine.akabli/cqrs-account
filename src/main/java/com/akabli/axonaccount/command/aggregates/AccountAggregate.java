package com.akabli.axonaccount.command.aggregates;

import com.akabli.axonaccount.command.exceptions.ExceptionControllerAdvice;
import com.akabli.axonaccount.commonapi.commands.CreateAccountCommand;
import com.akabli.axonaccount.commonapi.commands.CreditAccountCommand;
import com.akabli.axonaccount.commonapi.commands.DebitAccountCommand;
import com.akabli.axonaccount.commonapi.enums.AccountStatus;
import com.akabli.axonaccount.commonapi.events.AccountCreatedEvent;
import com.akabli.axonaccount.commonapi.events.AccountCreditedEvent;
import com.akabli.axonaccount.commonapi.events.AccountDebitedEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate // required par axon
public class AccountAggregate {
    // REPRESENTE L'ETAT ACTUEL DU COMPTE
    @AggregateIdentifier
    private String accountId;
    private String currency;
    private double balance;
    private AccountStatus status;

    public AccountAggregate(){
    }

    @CommandHandler // Function de decision
    public AccountAggregate(CreateAccountCommand command){
        if(command.getInitialeBalance() < 0) throw new ExceptionControllerAdvice.BalanceNegativeException("balance is negative");
        AggregateLifecycle.apply(new AccountCreatedEvent(
                command.getId(),
                command.getCurrency(),
                command.getInitialeBalance(),
                AccountStatus.CREATED
        ));
    }

    // fonction d'evolution
    @EventSourcingHandler
    public void on(AccountCreatedEvent event){
        this.accountId = event.getId();
        this.currency = event.getCurrency();
        this.balance = event.getBalance();
        this.status = event.getStatus();
    }

    @CommandHandler
    public void handle(CreditAccountCommand command){
        if(command.getAmount() < 0) throw new ExceptionControllerAdvice.BalanceNegativeException("balance is negative");
        /*
            regles metier
         */
        AggregateLifecycle.apply(new AccountCreditedEvent(
                command.getId(),
                command.getCurrency(),
                command.getAmount()
        ));
    }

    @EventSourcingHandler
    public void on(AccountCreditedEvent event){
        this.balance+=event.getAmount();
    }

    @CommandHandler
    public void handle(DebitAccountCommand command){
        if(command.getAmount() < 0) throw new ExceptionControllerAdvice.BalanceNegativeException("balance is negative");
        if(command.getAmount() > this.balance) throw new ExceptionControllerAdvice.BalanceNegativeException("solde is not enough");
        /*
            regles metier
         */
        AggregateLifecycle.apply(new AccountDebitedEvent(
                command.getId(),
                command.getCurrency(),
                command.getAmount()
        ));
    }

    @EventSourcingHandler
    public void on(AccountDebitedEvent event){
        this.balance-=event.getAmount();
    }
}
