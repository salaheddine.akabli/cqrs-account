package com.akabli.axonaccount.command.web;

import com.akabli.axonaccount.command.dto.CreateAccoutnDTO;
import com.akabli.axonaccount.command.dto.CreditAccoutnDTO;
import com.akabli.axonaccount.command.dto.DebitAccoutnDTO;
import com.akabli.axonaccount.commonapi.commands.CreateAccountCommand;
import com.akabli.axonaccount.commonapi.commands.CreditAccountCommand;
import com.akabli.axonaccount.commonapi.commands.DebitAccountCommand;
import lombok.AllArgsConstructor;
import org.axonframework.commandhandling.gateway.CommandGateway;
import org.axonframework.commandhandling.gateway.CommandGatewayFactory;
import org.axonframework.eventsourcing.eventstore.EventStore;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Stream;

@RestController
@RequestMapping("/command/account")
@AllArgsConstructor
public class AccountCommandController {
    private final CommandGateway commandGateway;
    private final EventStore eventStore;

    @PostMapping("/create")
    public CompletableFuture<String> createAccount(@RequestBody CreateAccoutnDTO request){
        return commandGateway.send(new CreateAccountCommand(
                UUID.randomUUID().toString(),
                request.getCurrency(),
                request.getBalance()
        ));
    }

    @PostMapping("/debit")
    public CompletableFuture<String> debitAccount(@RequestBody DebitAccoutnDTO request){
        return commandGateway.send(new DebitAccountCommand(
                request.getAccountId(),
                request.getCurrency(),
                request.getAmount()
        ));
    }

    @PostMapping("/credit")
    public CompletableFuture<String> creditAccount(@RequestBody CreditAccoutnDTO request){
        return commandGateway.send(new CreditAccountCommand(
                request.getAccountId(),
                request.getCurrency(),
                request.getAmount()
        ));
    }

    @GetMapping("/eventstore/{id}")
    public Stream readEvent(@PathVariable String id){
        return eventStore.readEvents(id).asStream();
    }

}
