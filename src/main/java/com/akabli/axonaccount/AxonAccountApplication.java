package com.akabli.axonaccount;

import com.akabli.axonaccount.config.AxonConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(AxonConfig.class)
public class AxonAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(AxonAccountApplication.class, args);
    }

}
